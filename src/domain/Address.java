package domain;

public class Address extends Entity{
	private String countryID;
	private String regionID;
	private String city;
	private String street;
	private String houseNumber;
	private String localNumber;
	private String zipCode;
	private String typeID;
	
	public Address (String countryID, String regionID, String city, String street, String houseNumber, String localNumber, String zipCode, String typeID){
		this.countryID = countryID;
		this.regionID = regionID;
		this.city = city;
		this.street = street;
		this.houseNumber = houseNumber;
		this.localNumber = localNumber;
		this.zipCode = zipCode;
		this.typeID = typeID;
	}

	public String getCountryID() {
		return countryID;
	}

	public void setCountryID(String countryID) {
		this.countryID = countryID;
	}

	public String getRegionID() {
		return regionID;
	}

	public void setRegionID(String regionID) {
		this.regionID = regionID;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getLocalNumber() {
		return localNumber;
	}

	public void setLocalNumber(String localNumber) {
		this.localNumber = localNumber;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getTypeID() {
		return typeID;
	}

	public void setTypeID(String typeID) {
		this.typeID = typeID;
	}

}
