package domain;

public class User extends Entity{
	private String login;
	private String password;
	
	public User(int id, String login, String password) {
		this.id = id;
		this.login = login;
		this.password = password;
	}
	public User(int id, EntityState state, String login, String password) {
		this.id = id;
		this.state = state;
		this.login = login;
		this.password = password;
	}
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}