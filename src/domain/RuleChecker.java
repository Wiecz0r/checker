package domain;

import java.util.ArrayList;
import java.util.List;

public class RuleChecker<TEntity> {
	private List<CheckRule<TEntity>> rules;

	public List<CheckRule<TEntity>> getRules() {
		return rules;
	}

	public void setRules(List<CheckRule<TEntity>> rules) {
		this.rules = rules;
	}
	public List<CheckResult> check(TEntity entity){
		List<CheckResult> result = new ArrayList<CheckResult>();
		for(CheckRule<TEntity> rule: rules){
			result.add(rule.checkRule(entity));
		}
		return result;
	}
}
