package domain;

public interface CheckRule<TEntity>{
	public CheckResult checkRule(TEntity entity);
}
