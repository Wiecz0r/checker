package ruletest;

import org.junit.Assert;
import org.junit.Test;

import rule.SurnameRule;
import domain.CheckResult;
import domain.Person;
import domain.RuleResult;

public class SurnameRuleTest {

public SurnameRule rule = new SurnameRule();
	
	@Test
	public void checker_should_check_if_the_person_surname_is_not_null(){
		Person p = new Person();
		CheckResult result = rule.checkRule(p);
		Assert.assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_check_if_the_person_surname_is_not_empty(){
		Person p = new Person();
		p.setSurname("");
		CheckResult result = rule.checkRule(p);
		Assert.assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_return_OK_if_the_surname_is_not_null(){
		Person p = new Person();
		p.setSurname("Nazwisko");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Ok, result.getResult());
		
	}
}
