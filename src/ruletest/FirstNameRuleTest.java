package ruletest;

import org.junit.Assert;
import org.junit.Test;

import rule.FirstNameRule;
import domain.CheckResult;
import domain.Person;
import domain.RuleResult;

public class FirstNameRuleTest {

public FirstNameRule rule = new FirstNameRule();
	
	@Test
	public void checker_should_check_if_the_person_name_is_not_null(){
		Person p = new Person();
		CheckResult result = rule.checkRule(p);
		Assert.assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_check_if_the_person_name_is_not_empty(){
		Person p = new Person();
		p.setFirsName("");
		CheckResult result = rule.checkRule(p);
		Assert.assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_return_OK_if_the_name_is_not_null(){
		Person p = new Person();
		p.setFirsName("Imie");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Ok, result.getResult());
		
	}
}