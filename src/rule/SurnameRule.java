package rule;

import domain.CheckResult;
import domain.CheckRule;
import domain.Person;
import domain.RuleResult;

public class SurnameRule implements CheckRule<Person>{
	
	public CheckResult checkRule(Person entity){
		
		if(entity.getSurname()==null)
			return new CheckResult("", RuleResult.Error);
		if(entity.getSurname().equals(""))
			return new CheckResult("", RuleResult.Error);
		return new CheckResult("",RuleResult.Ok);
	}
}
