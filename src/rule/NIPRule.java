package rule;

import domain.CheckResult;
import domain.CheckRule;
import domain.Person;
import domain.RuleResult;

public class NIPRule implements CheckRule<Person>{

public CheckResult checkRule(Person entity){
	int suma = 10;
		
	try {
		suma = 6*Character.getNumericValue(entity.getNip().charAt(0)) + 5*Character.getNumericValue(entity.getNip().charAt(1)) + 7*Character.getNumericValue(entity.getNip().charAt(2)) + 
				2*Character.getNumericValue(entity.getNip().charAt(3)) + 3*Character.getNumericValue(entity.getNip().charAt(4)) + 4*Character.getNumericValue(entity.getNip().charAt(5)) +
				5*Character.getNumericValue(entity.getNip().charAt(6)) + 6*Character.getNumericValue(entity.getNip().charAt(7)) + 7*Character.getNumericValue(entity.getNip().charAt(8));
		suma = suma%11;
		if (suma == Character.getNumericValue(entity.getNip().charAt(9)))
			return new CheckResult("",RuleResult.Ok);
		else
			return new CheckResult("",RuleResult.Error);
	}catch (Exception e){
		return new CheckResult("",RuleResult.Error);
	}
	}
}
