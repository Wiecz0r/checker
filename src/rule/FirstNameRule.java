package rule;

import domain.CheckResult;
import domain.CheckRule;
import domain.Person;
import domain.RuleResult;

public class FirstNameRule implements CheckRule<Person>{
	
	public CheckResult checkRule(Person entity){
		
		if(entity.getFirsName()==null)
			return new CheckResult("", RuleResult.Error);
		if(entity.getFirsName().equals(""))
			return new CheckResult("", RuleResult.Error);
		return new CheckResult("",RuleResult.Ok);
	}
}
